import { ApiRequest } from "../controllers/request";

export class DeleteUser {
    async delete_user(id) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("DELETE")
            .url(`api/Users/${id}`)
            .send();
        return response;
    }
}