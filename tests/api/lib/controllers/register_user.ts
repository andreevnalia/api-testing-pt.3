import { ApiRequest } from "../controllers/request";

export class RegisterUser {
    async register(emailValue: string, passwordValue: string, userName: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Register`)
            .body({
                email: emailValue,
                password: passwordValue,
                userName: userName,
            })
            .send();
        return response;
    }
}