import { ApiRequest } from "../controllers/request";

export class Get_Info_About_User_From_Token {
    async userToken(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("GET")
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}