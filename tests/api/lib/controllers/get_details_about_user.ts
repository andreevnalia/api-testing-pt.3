import { ApiRequest } from "../controllers/request";

export class GetDetailsAboutUser {
    async getUserById(id) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("GET")
            .url(`api/Users/${id}`)
            .send();
        return response;
    }
}