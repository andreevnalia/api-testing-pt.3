import { ApiRequest } from "../controllers/request";

export class GetAllUsers {
    async getAllUsers() {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("GET")
            .url(`api/Users`)
            .send();
        return response;
    }
}